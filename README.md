# SpringMVCApp



## Spring MVC learning

1.	Spring MVC. Первое приложение.

2.	Spring MVC. Конфигурация с помощью Java кода.

3.	Контроллеры. Аннотация @Controller.

4.	Параметры GET запроса. Аннотация @RequestParam.

5.	Модель. Передача данных от контроллера к представлению.

6.	CRUD, REST, Паттерн DAO (Data Access Object).

7.	Аннотация @ModelAttribute. HTML Формы (Thymeleaf).

8.	CRUD приложение. PATCH, DELETE запросы.

9.	Валидация форм. Аннотация @Valid.

10.	JDBC API. Базы данных.

11.	SQL инъекции. PreparedStatement. JDBC API.

12.	JdbcTemplate.
